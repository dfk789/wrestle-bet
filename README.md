Wrestle-Bet is an express application that will eventually be a fire pro themed clone of saltybet.

The goal of this project is to familiarise myself better with asynchronus code, backend development, express and a postgres database. I may implement mongodb as an option to also get familiar with. 

Functionality is very minimal at the moment.

**Requires a key.pem and certificate.pem in ./config**

**Requires a postgres database**

You can put your postgres database settings in ./config/db_config.json.

**the application will create the tables for users and world-champs with some example data**

You can register new users at /register. Passwords get salted and hashed with argon2id. The form has some light validation. 

You can create new world-champs at /champions/world.


npm install & npm start should get you going, if you have provided the correct things.