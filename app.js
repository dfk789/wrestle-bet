var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const session = require('express-session')
//const ChampQueries = require('./champ_queries')
//const UserQueries = require('./user_queries')
const DB = require('./db/index')
const db = new DB()
db.initialise()


var indexRouter = require('./routes/index');
const landingRouter = require('./routes/user/landing')
const authRouter = require('./routes/auth/index')
const registerRouter = require('./routes/register/register')
const championsRouter = require('./routes/champions/index')
const betRouter = require('./routes/bets/index')

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  key:'user_sid',
  signed:true,
  secret:'badman',
  name:'testsess',
  saveUninitialized: false,
  resave:false,
  pageViews:0, 
  cookie:{
    maxAge:60000 * 10,
    secure:true}
  }))

app.use('/', indexRouter);
app.use('/', authRouter(db))
app.use('/landing', landingRouter(db))
app.use('/register', registerRouter(db))
app.use('/champions', championsRouter(db))
app.use('/bets', betRouter(db))


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
