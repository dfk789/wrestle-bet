class ChampQueries {
  constructor(pool) {
    this.pool = pool
  }
}


ChampQueries.prototype.uniqueId = async function (fpid) {
  const champIds = await this.pool.query(`SELECT name, fpid FROM "world-champions" WHERE fpid = '${fpid}'`)
  if(!champIds.rows[0]) return true
  return false
}

ChampQueries.prototype.getWorldChamps = function (limit) {
  limit ? limit = limit : limit = 10
  return this.pool.query(`SELECT (name, won, defences, fpid) FROM "world-champions" ORDER BY current DESC, defences DESC LIMIT ${limit}`)
};

ChampQueries.prototype.noChamp = function () {
  return this.pool.query(`UPDATE "world-champions" SET current = false`)
}

ChampQueries.prototype.newChamp = function (fpid) {
  return this.pool.query(`UPDATE "world-champions" SET current = true, won = won + 1 WHERE fpid = ${fpid}`)
}

ChampQueries.prototype.defended = function (fpid) {
  return this.pool.query(`UPDATE "world-champions" SET defences = defences + 1 WHERE fpid = ${fpid}`)
}

ChampQueries.prototype.debutChamp = function (wrestler) {
  return this.pool.query(`INSERT INTO "world-champions" (name, fpid) VALUES ($1, $2)`, [wrestler.name, wrestler.fpid])
}



module.exports = ChampQueries