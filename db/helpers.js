validData = (data) => {
  if (data.rows.length == 0) return false
  return data.rows[0]
}

module.exports = {validData}