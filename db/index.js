const Pool = require('pg').Pool
const ChampQueries = require('./champ_queries')
const UserQueries = require('./user_queries')
const {doesDBExist, doesTablesExist} = require('./initialise')
let dbConfig

try {dbConfig = require('../config/my_config')} catch {
  dbConfig = require('../config/db_config')
};

class Database {
  constructor() {
    this.pool = new Pool(dbConfig)
    this.champQueries = new ChampQueries(this.pool)
    this.userQueries = new UserQueries(this.pool)
  }
}

Database.prototype.initialise = async function () {
  await doesDBExist(this.pool)
  await doesTablesExist(this.pool)
}


module.exports = Database