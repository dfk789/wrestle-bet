const doesDBExist = async function (pool) {
  await pool.query('SELECT NOW()', (err) => {
    if (err) throw new Error (`Please make sure you have PostgresSQL installed and have your details in ./config/db_config.json`, err)
  })
}

const doesTablesExist = async function (pool) {
  await pool.query(`SELECT 'public.users'::regclass`, async function (err) {
    if (err) {
      await createUserTable(pool)
    }
  })
  await pool.query(`SELECT 'public.world-champions'::regclass`, async function (err) {
    if (err) {
      await createChampTable(pool)
    }
  })
}

const createUserTable = async function (pool) {
  await pool.query(`
    -- Table: public.users

    -- DROP TABLE public.users;
    
    CREATE TABLE public.users
    (	id bigserial NOT NULL,
        username character varying(30) COLLATE pg_catalog."default" NOT NULL,
        email character varying(51) COLLATE pg_catalog."default" NOT NULL,
        pwhash character varying(96) COLLATE pg_catalog."default" NOT NULL,
        cash smallint NOT NULL DEFAULT 5000,
        
        CONSTRAINT "unique email" UNIQUE (email)
    ,
        CONSTRAINT "unique username" UNIQUE (username)
    
    )
    WITH (
        OIDS = FALSE
    )
    TABLESPACE pg_default;
    
    COMMENT ON TABLE public.users
        IS 'wrestle-bet users, containing emails and hashed passwords';
        
  `)
  await createExampleUserData(pool)
}

const createChampTable = async function (pool) {
  await pool.query(`
    -- Table: public."world-champions"

    -- DROP TABLE public."world-champions";

    CREATE TABLE public."world-champions"
    (
        name character varying(50) COLLATE pg_catalog."default" NOT NULL,
        won bigint NOT NULL DEFAULT 1,
        id bigserial NOT NULL,
        fpid bigint NOT NULL,
        current boolean NOT NULL DEFAULT true,
        defences bigint NOT NULL DEFAULT 0,
        CONSTRAINT "world-champions_pkey" PRIMARY KEY (id),
        CONSTRAINT "unique fpid" UNIQUE (fpid)

    )
    WITH (
        OIDS = FALSE
    )
    TABLESPACE pg_default;

    COMMENT ON TABLE public."world-champions"
        IS 'winners of the top belt in wrestle-bet';
  `)
  await createExampleChampData(pool)
}

//dummy initial  user data for example/ badman/badpassword, 1/12
const createExampleUserData = async function (pool) {
  await pool.query(`
    INSERT INTO users (username, email, pwhash) VALUES 
      ('Badman', 'badman@mail.com', '$argon2id$v=19$m=4096,t=3,p=1$0XBqEG+Nm/q5XKdvb829dg$YK/cCYrd1gUz57vciEbB11pbnA1I4bTbGA4jcLzbSbY'),
      ('1', '1@12.com', '$argon2id$v=19$m=4096,t=3,p=1$GFWtBNNsO011+vs988r7cQ$6h9IxkMx1cKQwj8Zd7Q4RZlqYEwnMnjFrU5KmpOZeAg')
  `)
}

const createExampleChampData = async function (pool) {
  await pool.query(`
    INSERT INTO "world-champions" (name, won, fpid, current, defences) VALUES ($1, $2, $3, $4, $5)`,
    ["Kane 98'", 1, 545646564, false, 2])  
  await pool.query(`
    INSERT INTO "world-champions" (name, won, fpid, current, defences) VALUES ($1, $2, $3, $4, $5)`,
    ['Mark "Sexual Chocolate" Henry', 2,846516516, true, 9])
}

module.exports = {doesDBExist, doesTablesExist}