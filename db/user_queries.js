const {validData} = require('./helpers')
class UserQueries {
  constructor(pool) {
    this.pool = pool
  }
};


UserQueries.prototype.isUniqueEmail = async function (email) {
  const uniqueEmail = await this.pool.query(`SELECT email FROM users WHERE email ILIKE '${email}'`)
  if (uniqueEmail.rows.length > 0) return false
  return true
}

UserQueries.prototype.isUniqueUsername = async function (username) {
  const uniqueUsername = await this.pool.query(`SELECT username FROM users WHERE username ILIKE '${username}'`)
  if (uniqueUsername.rows.length > 0) return false
  return true
}

UserQueries.prototype.addUser = async function (newUser)  {
  return await this.pool.query('INSERT INTO users (username, email, pwhash) VALUES ($1, $2, $3)', [newUser.username, newUser.email, newUser.password])
}

UserQueries.prototype.getHash = async function (username) {
  const hash = await this.pool.query(`SELECT username, pwhash FROM users WHERE username ILIKE '${username}'`)
  return validData(hash) ? (validData(hash)).pwhash : false  
}

UserQueries.prototype.getUsername = async function (username) {
  const string = await this.pool.query(`SELECT username FROM users WHERE username ILIKE '${username}'`)
  if (string.rows.length) return string.rows[0].username
}

UserQueries.prototype.getUserId = async function (username) {
  const id = await this.pool.query(`SELECT id FROM users WHERE username ILIKE '${username}'`)
  return (validData(id)) ? (validData(id)).id : false
}

UserQueries.prototype.getCash = async function (username) {
  const cash = await this.pool.query(`SELECT username, cash FROM users WHERE username ILIKE '${username}'`)
  return (validData(cash)) ? (validData(cash)).cash : false
}

UserQueries.prototype.updateCash = function (username, amount) {
  return this.pool.query(`UPDATE users SET cash = ${amount} WHERE username ILIKE ${username}`)
}

module.exports = UserQueries