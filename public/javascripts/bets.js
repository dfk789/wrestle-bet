var xhr = new XMLHttpRequest();

function updateBetValue () {  
  document.getElementById('bet-value').innerText = document.getElementById('bet-value-slider').value
};

function betRed () {
 placeBet('red')
}

function betBlue () {
  placeBet('blue')
}

function placeBet (team) {
  xhr.open("POST", 'https://127.0.0.1:3801', true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify({
    value: document.getElementById('bet-value-slider').value,
    team:team
  }));
}

document.getElementById('bet-red').addEventListener('click',betRed)
document.getElementById('bet-blue').addEventListener('click',betBlue)

document.getElementById('bet-value-slider').addEventListener('change',updateBetValue)
document.getElementById('bet-value-slider').addEventListener('mousemove',updateBetValue)