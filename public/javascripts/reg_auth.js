let pass1 = document.querySelector('#reg-pass')
let pass2 = document.querySelector('#reg-pass2')
let email = document.querySelector('#reg-email')
let regButton = document.querySelector('#reg-button')


function checkPass () {
  if (pass2.value != pass1.value) {
    pass2.setCustomValidity('Passwords must match.')
  } else { 
    pass2.setCustomValidity('')
  }
};

function validateEmail () {
  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

  if (!re.test(email.value))  {
    email.setCustomValidity('Please enter a valid Email.')
  } else {
    email.setCustomValidity('')
  }
};

function validateAll () {
  validateEmail()
  checkPass()
}


//pass2.setCustomValidity('Passwords must match.')

email.addEventListener('keyup', validateEmail)
pass2.addEventListener('keyup', checkPass)


// console.log(pass1)
// console.log(pass2)

