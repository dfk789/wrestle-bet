const argon2 = require('argon2')

argon2Hash = async function (password) {
  try {
    if (typeof password != 'string') return
    return await argon2.hash(password, {type:argon2.argon2id})
  } catch (error) {
    console.log(error)
  }
};

veriHash = async function (hashedPW, password) {
  try {
    return await argon2.verify(hashedPW, password)
  } catch (error) {
    console.log('verihash error:', error)
  }
};


module.exports = {argon2Hash, veriHash}