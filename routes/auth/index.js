var express = require('express');
var router = express.Router();
const loginRoute = require('./login/index')
const logoutRoute = require('./logout')

authRoute = db => {
  router.use('/login', loginRoute(db))

  router.use('/logout', logoutRoute)

  return router
}


module.exports = authRoute