var express = require('express');
var router = express.Router();
const {getPasswordHash,verifyPassword,createSession} = require('./login')

loginRoute = db => {
  router.get('/', function(req, res, next) {
    res.redirect('/')
  })

  router.post('/', getPasswordHash(db))
  router.post('/', verifyPassword)
  router.post('/', createSession(db))

  router.post('/', async function (req, res, next) {
    console.log(req.session)
    res.redirect('landing')
  })

  return router
}


module.exports = loginRoute
