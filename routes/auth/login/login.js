
const {veriHash} = require('../crypto')


const getPasswordHash = (db) => async (req, res, next) => {
  try {
    const hash = await db.userQueries.getHash(req.body.username)
    if (!hash) return res.status(401).render('index', {userError:'Cannot find user.' }) 
    req.session.hash = hash
    res.status(200)
    next()
  } catch (error) {
    next(error)
  }
}

const verifyPassword = async (req, res, next) => {
  try {
    if (!await veriHash(req.session.hash, req.body.password)) return res.status(401).render('index', {passwordError:'Wrong Password' })
    res.status(200)
    next()
  } catch (error) {
    next(error)
  }
}

const createSession = (db) => async (req, res, next) => {
  console.log('session id =', req.sessionID)
  req.session.authenticated = true
  req.session.username = await db.userQueries.getUsername(req.body.username)
  delete req.session.hash
  req.session.cash = await db.userQueries.getCash(req.body.username)
  res.status(200)
  next()
}

module.exports = {getPasswordHash, verifyPassword, createSession}