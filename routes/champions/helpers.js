//parses data query of get champs.
parseChampions = function (champData) {
  try {
    const champRegEx = /(.+),(.+),(.+),(.+)/
    let champArray = []
    champArray.push(champData.map(champion => {
      let champ = {},
      champSplit = champion.row.split(champRegEx)
        champ.name = champSplit[1].replace('(','')
        champ.won = champSplit[2]
        champ.defences = champSplit[3]
        champ.fpid = champSplit[4].replace(')','')
      return champ
    }))
    return champArray
  } catch (error) {
    console.log(error)
  }
};

newChampion = async function (db, wrestler) {
  try {
    await db.champQueries.noChamp()
    if (await db.champQueries.uniqueId(wrestler.fpid)) {
      return await db.champQueries.debutChamp(wrestler)      
    } else {
      return await db.champQueries.newChamp(wrestler.fpid)     
    }
  } catch (error) {
    console.log('new champion error', error)
  }
}

defended = async function (db, fpid) {
  try{
    return await db.champQueries.defended(fpid)
  } catch (error) {
    console.log(error)
  }
}

module.exports = {parseChampions, newChampion, defended}