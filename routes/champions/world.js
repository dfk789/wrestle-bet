var express = require('express');
var router = express.Router();
const {parseChampions, newChampion} = require('./helpers')

worldChampRoute = db => {

  router.get(`/:amount(\\d+)/`, async function (req, res, next) {
    let worldChamps = await db.champQueries.getWorldChamps(req.params.amount)
    worldChamps = parseChampions(worldChamps.rows)
    res.render('champions', {
      belt:'World Heavyweight',
      currentChamp:worldChamps[0][0].name,
      champs:worldChamps[0]
    })
  })  
  
  router.get('/', async (req, res, next) => {
    let worldChamps = await db.champQueries.getWorldChamps()
    worldChamps = parseChampions(worldChamps.rows)
    res.render('champions', {
      belt:'World Heavyweight',
      currentChamp:worldChamps[0][0].name,
      champs:worldChamps[0]
    })
  })

  router.post('/', async function (req, res, next) {
    console.log(req.body)
    newChampion(db,req.body)
    res.redirect('/champions/world')
  })

  return router
}

module.exports = worldChampRoute

