const {argon2Hash} = require('../auth/crypto')

class RegValidation {
  constructor(UserQueries){
    this.userQueries = UserQueries
  }
};

//dont use this? if a user circumvents client side verification for matching passwords...
//not my problem? and could be security issue if used?
RegValidation.prototype.passMatch = function (pass1, pass2) {
  if (pass2 != pass1) return false
  return true
};

RegValidation.prototype.isValidEmail = async function (email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (!email) return false
  if (!re.test(email)) return false
  return true
};

RegValidation.prototype.isValidUsername = async function (username) {
  if (!username) return false
  if (!/^[A-Za-z0-9]+(?:[_-][A-Za-z0-9]+)*$/.test(username)) return false
  return true
};

RegValidation.prototype.isUniqueUsername = async function (username) {
  return await this.userQueries.isUniqueUsername(username)

};

RegValidation.prototype.isUniqueEmail = async function (email) {
  return await this.userQueries.isUniqueEmail(email)
};

RegValidation.prototype.badPost = function () {
  if (!this.passMatch()) res.render
};

RegValidation.prototype.validFormData = async function (postData) {
  if (!await this.isValidUsername(postData.username)) return false
  if (!await this.isValidEmail(postData.email)) return false
  //if(!x.passMatch(newUser.password, newUser.confirmPassword)) return false
  return true
};

RegValidation.prototype.createNewUser = async function (postData) {
  try {
    const newUser = {username:postData.username, email:postData.email, password:await argon2Hash(postData.password)}
    return newUser
  } catch (error) {
    console.log(`createnewuser caught error`, error)
  };
};

RegValidation.prototype.addNewUser = async function (newUser) {
  try {
   return await this.userQueries.addUser(newUser)
  }
  catch (error) {
    console.log('addnewuser caught error', error)
  };
};

module.exports = RegValidation