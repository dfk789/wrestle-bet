var express = require('express');
var router = express.Router();
const RegValidation = require('./reg_validation')

const registerRoute = db => {
  const regValidation = new RegValidation(db.userQueries)

  router.get('/', function(req, res, next) {
    res.render('register')
  });
  
  //check formdata is good before db stuff
  router.post('/', async function (req, res, next) {
    if (!await regValidation.validFormData(req.body)) {
      return res.render('register', {usernameError:'There was a problem with your registration'})
    } 

    next() 
  })

  //check username and email are unique before adding
  router.post('/', async function (req, res, next) {
    if (!await regValidation.isUniqueUsername(req.body.username)) {
      return res.render('register', {usernameError:'This username is already taken.'})
    }
    
    if (!await regValidation.isUniqueEmail(req.body.email)) {
      return res.render('register', {emailError:'This email is already taken.'})
    }

    next()
  })

  //add new user to db, redirect to index
  router.post('/', async function (req, res, next) {
    const newUser = await regValidation.createNewUser(req.body)
    try {
      await db.userQueries.addUser(newUser)
      res.redirect('/')
    } catch (error) {
      res.send('there was a problem with your registration: ' +  error.name)
      console.log('registration error', error)  
      next(error)    
    }
  })
    
  return router
}



module.exports = registerRoute;
