const {argon2Hash} = require('../../../routes/auth/crypto')
const hashRegEx = /\$argon2id\$v=19\$m=4096,t=3,/


describe('argon2Hash function test', () => {

  test('string returns argon2id hashed pw', async () => {
    const pwString = '123'
    const hash = await argon2Hash(pwString)
    expect(hashRegEx.test(hash)).toBe(true) 
  })

  test('null password is not a valid hash', async () => {
    const pwString = null
    const hash = await argon2Hash(pwString)
    console.log(hash)
    expect(hashRegEx.test(hash)).toBe(false) 
  })

})