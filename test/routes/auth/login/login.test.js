const {getPasswordHash, verifyPassword, createSession} = require('../../../../routes/auth/login/login')
const pwHash = '$argon2id$v=19$m=4096,t=3,p=1$HAfw1EGJvIv78t9nPmr4iQ$n4OrI7sPSEogg1+rY/Seq4/zUwDJun4FWg2yt9YlqvM'
const DB = require('../../mockDB/mockInit')
const db = new DB()

//mockrequest takes user password and hash.
const mockRequest = (user, password, hash) => {
  return {
    session:{
      hash:hash
    },
    body:{
      username:user,
      password:password
    }
  }
};

//mock response, mock res.status and return value to check in tests
const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

//mock next doesnt need to do anything. could jest mock it to see if its called. 
//but we just check the status are called in res instead.
const next = () => {}


describe('getPasswordHash function test', () => {
  test('valid user gets hash and 200s', async () => {
    const req = mockRequest('1','123')
    const res = mockResponse()
    const next = () => {}
    await getPasswordHash(db)(req,res,next)
    expect(res.status).toHaveBeenCalledWith(200)
  })

  test('invalid users return 401s', async () => {
    const req = mockRequest('1123213','123')
    const res = mockResponse()
    const next = () => {}
    await getPasswordHash(db)(req,res,next)
    expect(res.status).toHaveBeenCalledWith(401)
  })
  
})

describe('verifyPassword function test', () => {
  test('200status correct password', async () => {
    const req = mockRequest('1','123',pwHash)
    const res = mockResponse()
    const next = () => {}
    await verifyPassword(req, res,next)
    expect(res.status).toHaveBeenCalledWith(200)
  })
  
  test('401status incorrect password', async () => {
    const req = mockRequest('1','1',pwHash)
    const res = mockResponse()
    const next = () => {}
    await verifyPassword(req, res, next)
    expect(res.status).toHaveBeenCalledWith(401)
  })
})


test('createSession function test', async () => {
  const req = mockRequest('1','12',pwHash)
  const res = mockResponse()
  const next = () => {}
  await createSession(db)(req,res,next)
  console.log(req.session)
  expect(res.status).toHaveBeenCalledWith(200)
})



