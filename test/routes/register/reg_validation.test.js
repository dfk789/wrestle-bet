const RegValidation = require('../../../routes/register/reg_validation')
const regValidation = new RegValidation()


let mockForm = {

}
const mockUser = {
  username:'redman',
  email:'redman@email.com',
  password:'123'
}



describe('RegValidation methods test', () => {

  test('passMatch method  returns correct bools', () => {
    expect (regValidation.passMatch('12','123')).toBe(false)
    expect (regValidation.passMatch(null, null)).toBe(true)
    expect (regValidation.passMatch('12','12')).toBe(true)
  })

  test('isValidEmail returns correct bools', async () => {
    expect (await regValidation.isValidEmail(null)).toBe(false)
    expect (await regValidation.isValidEmail(undefined)).toBe(false)
    expect (await regValidation.isValidEmail('12@12.12')).toBe(false)
    expect (await regValidation.isValidEmail('re@red.com')).toBe(true)
  })

  test('validFormData returns correct(true) bool with valid data', async () => {
    mockForm.username = 'redman',
    mockForm.email = 'redman@gmail.com'
    expect (await regValidation.validFormData(mockForm)).toBe(true)
  })

  test('validFormData returns correct(false) bool with invalid username', async () => {
    mockForm.username = null,
    mockForm.email = 'redman@gmail.com',
    expect (await regValidation.validFormData(mockForm)).toBe(false)
  })

  test('validFormData returns correct(false) bool with invalid email', async () => {
    mockForm.username = 'redman',
    mockForm.email = null
    expect (await regValidation.validFormData(mockForm)).toBe(false)
  })
  
})










// test('create new user, returns user with hashed pw', async () => {
//   mockForm.username = 'redman',
//   mockForm.email = 'redman@email.com'
//   mockForm.password = '123'
//   expect(
//      await regValidation.createNewUser(mockForm)
//   ).toEqual({
//     username:'redman',
//     email:'redman@email.com',
//     password: expect.anything()
//   })
// })



